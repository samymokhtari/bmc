package com.samy.bmc.electricTerminals.data.remote

import com.samy.bmc.electricTerminals.data.model.ElectricTerminalsRetrofit
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query


interface IElectricTerminalsEndpoint {

    /**
     * Get a list of electric terminal around you given a region, geo point and a radius
     */

    @GET("search/")
    fun getElectricTerminals(
        @Query("dataset") dataset: String = "bornes-irve",
        @Query("facet") facets: List<String> = listOf("region", "departement"),
        @Query("refine.region") region: String = "Hauts-de-France",
        @Query("geofilter.distance") geofilterDistance: String,
        @Query("rows") rows: Int = 30
    ): Call<ElectricTerminalsRetrofit>

}