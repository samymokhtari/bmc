package com.samy.bmc.electricTerminals.domain.model

import com.google.gson.annotations.SerializedName

/***
 *
 */
data class ElectricTerminalsDomain(
    @SerializedName("xlongitude"         ) var xlongitude       : Double,
    @SerializedName("acces_recharge"     ) var accesRecharge    : String,
    @SerializedName("date_maj"           ) var dateMaj          : String,
    @SerializedName("ad_station"         ) var adStation        : String,
    @SerializedName("ylatitude"          ) var ylatitude        : Double
)
/** Object use for Domain part */
//data class ElectricTerminalsDomain(
//    @SerializedName("nhits"        ) var nhits       : Int,
//    @SerializedName("records"      ) var records     : List<Records>     = arrayListOf(),
//)
//
//data class Fields (
//    @SerializedName("xlongitude"         ) var xlongitude       : Double,
//    @SerializedName("acces_recharge"     ) var accesRecharge    : String,
//    @SerializedName("date_maj"           ) var dateMaj          : String,
//    @SerializedName("ad_station"         ) var adStation        : String,
//    @SerializedName("ylatitude"          ) var ylatitude        : Double
//)
//
//data class Records (
//    @SerializedName("fields"           ) var fields          : Fields
//)