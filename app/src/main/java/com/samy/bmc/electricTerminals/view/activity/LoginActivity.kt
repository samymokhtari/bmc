package com.samy.bmc.electricTerminals.view.activity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View.INVISIBLE
import android.view.View.VISIBLE
import android.widget.Button
import android.widget.Toast
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.FirebaseAuthUIActivityResultContract
import com.firebase.ui.auth.data.model.FirebaseAuthUIAuthenticationResult
import com.google.firebase.auth.FirebaseAuth
import com.samy.bmc.R

class LoginActivity : AppCompatActivity() {

    private lateinit var btnLogInGoogle : Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        btnLogInGoogle = findViewById(R.id.btnLoginGoogle)

        val providers = arrayListOf(
            AuthUI.IdpConfig.GoogleBuilder().build()
        )

        // If we are already logged-in
        if (FirebaseAuth.getInstance().currentUser != null) {
            val user = FirebaseAuth.getInstance().currentUser
            Toast.makeText(this, String.format("Welcome %s!", user?.displayName), Toast.LENGTH_SHORT).show()
            goToMainActivity()
        }

        btnLogInGoogle.setOnClickListener {
            // Create and launch sign-in intent
            val signInIntent = AuthUI.getInstance()
                .createSignInIntentBuilder()
                .setAvailableProviders(providers)
                .build()
            signInLauncher.launch(signInIntent)
        }
    }

    private val signInLauncher = registerForActivityResult(
        FirebaseAuthUIActivityResultContract()
    ) { res ->
        this.onSignInResult(res)
    }

    private fun onSignInResult(result: FirebaseAuthUIAuthenticationResult) {
        val response = result.idpResponse
        if (result.resultCode == RESULT_OK) {
            // Successfully signed in
            val user = FirebaseAuth.getInstance().currentUser
            Toast.makeText(this, String.format("Welcome %s!", user?.displayName), Toast.LENGTH_SHORT).show()

            goToMainActivity()
        } else {
            // Sign in failed. If response is null the user canceled the
            // sign-in flow using the back button. Otherwise check
            // response.getError().getErrorCode() and handle the error.
            Toast.makeText(this, "Signing-in failed.", Toast.LENGTH_SHORT).show()
        }
    }

    private fun goToMainActivity() {
        startActivity(Intent(this, HomeActivity::class.java))
    }
}