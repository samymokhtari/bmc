package com.samy.bmc.electricTerminals.view.model

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.samy.bmc.databinding.ItemCustomRecyclerBinding
import com.samy.bmc.databinding.ItemCustomRecyclerViewFooterBinding
import com.samy.bmc.databinding.ItemCustomRecyclerViewHeaderBinding

class ElectricTerminalsViewHolder(
    private val binding: ItemCustomRecyclerBinding,
    onItemClick: (objectDataSample: ObjectDataSample, view: View) -> Unit
) : RecyclerView.ViewHolder(binding.root) {

    private lateinit var ui: ObjectDataSample

    init {
        binding.root.setOnClickListener {
            onItemClick(ui, itemView)
        }
    }

    fun bind(objectDataSample: ObjectDataSample) {
        ui = objectDataSample
        binding.itemRecyclerViewInfo1.text = objectDataSample.adStation
        binding.itemRecyclerViewInfo2.text = objectDataSample.accesRecharge
        binding.itemRecyclerViewInfo3.text = "Date de mise à jour: ${objectDataSample.dateMaj}"
    }
}

class ElectricTerminalsHeaderViewHolder(
    private val binding: ItemCustomRecyclerViewHeaderBinding,
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(objectDataSample: ObjectDataHeaderSample) {
        binding.itemRecyclerViewHeader.text = objectDataSample.header
    }
}

class ElectricTerminalsFooterViewHolder(
    private val binding: ItemCustomRecyclerViewFooterBinding,
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(objectDataSample: ObjectDataFooterSample) {
        binding.itemRecyclerViewFooter.text = objectDataSample.footer
    }
}