package com.samy.bmc.electricTerminals.view.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import androidx.lifecycle.viewModelScope
import com.google.android.gms.maps.model.LatLng
import com.samy.bmc.electricTerminals.data.repository.ElectricTerminalsRepository
import com.samy.bmc.electricTerminals.domain.usecase.GetElectricTerminalsUseCase
import com.samy.bmc.electricTerminals.view.mapper.fromDomainToUi
import com.samy.bmc.electricTerminals.view.mapper.toUi
import com.samy.bmc.electricTerminals.view.mapper.toMyObjectForRecyclerView
import com.samy.bmc.electricTerminals.view.model.ElectricTerminalsUi
import com.samy.bmc.electricTerminals.view.model.MyObjectForRecyclerView
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ElectricTerminalsViewModel : ViewModel() {

    private val getElectricTerminalsUseCase: GetElectricTerminalsUseCase by lazy { GetElectricTerminalsUseCase() }

    var electricTerminalsLiveData: LiveData<List<ElectricTerminalsUi>> =
        getElectricTerminalsUseCase.selectAll().map {
            it.fromDomainToUi()
        }

    fun initializeTerminals(geo: LatLng, distance: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            getElectricTerminalsUseCase.insertTerminals("${geo.latitude},${geo.longitude},${distance}")
        }

    }

    val electricTerminalsList: LiveData<List<MyObjectForRecyclerView>> =
        getElectricTerminalsUseCase.selectAll().map { list ->
            list.map {
                it.toUi()
            }.toMyObjectForRecyclerView()
        }
}