package com.samy.bmc.electricTerminals.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/** Object use for room */
@Entity(tableName = "electric_terminals")
data class ElectricTerminalsRoom(
    @ColumnInfo(name = "xlongitude")
    val xlongitude: Double,

    @ColumnInfo(name = "acces_recharge")
    val accesRecharge: String,

    @ColumnInfo(name = "date_maj")
    val dateMaj: String,

    @ColumnInfo(name = "ad_station")
    val adStation: String,

    @ColumnInfo(name = "ylatitude")
    val ylatitude: Double,

) {
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0
}

/** Object use for retrofit */
data class ElectricTerminalsRetrofit(
    @SerializedName("nhits")
    @Expose
    val nhits: Int,

    @SerializedName("records")
    @Expose
    val records: List<Record>
)

data class Record(
    @SerializedName("fields")
    @Expose
    val fields: Fields
)

data class Fields(
    @SerializedName("xlongitude")
    @Expose
    val xlongitude: Double,

    @SerializedName("acces_recharge")
    @Expose
    val accesRecharge: String,

    @SerializedName("date_maj")
    @Expose
    val dateMaj: String,

    @SerializedName("ad_station")
    @Expose
    val adStation: String,

    @SerializedName("ylatitude")
    @Expose
    val ylatitude: Double
)

