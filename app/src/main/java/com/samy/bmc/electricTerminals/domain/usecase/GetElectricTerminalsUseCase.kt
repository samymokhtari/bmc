package com.samy.bmc.electricTerminals.domain.usecase

import androidx.lifecycle.LiveData
import com.samy.bmc.electricTerminals.data.repository.ElectricTerminalsRepository
import com.samy.bmc.electricTerminals.domain.model.ElectricTerminalsDomain
import com.samy.bmc.electricTerminals.domain.repository.IElectricTerminalsRepository


class GetElectricTerminalsUseCase {
    private val electricTerminalsRepository: IElectricTerminalsRepository by lazy { ElectricTerminalsRepository() }

    fun selectAll(): LiveData<List<ElectricTerminalsDomain>> {
        return electricTerminalsRepository.selectAll()
    }

    suspend fun insertTerminals(geoPoint: String) {
        val rooms = electricTerminalsRepository.getTerminalsFromAPI(geoPoint)
        electricTerminalsRepository.insertTerminals(rooms)
    }
}