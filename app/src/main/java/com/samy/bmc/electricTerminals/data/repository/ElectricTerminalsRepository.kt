package com.samy.bmc.electricTerminals.data.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.map
import com.samy.bmc.architecture.CustomApplication
import com.samy.bmc.architecture.RetrofitBuilder
import com.samy.bmc.electricTerminals.data.local.ElectricTerminalsDao
import com.samy.bmc.electricTerminals.data.mapper.fromRoomToDomain
import com.samy.bmc.electricTerminals.data.mapper.toRoomObject
import com.samy.bmc.electricTerminals.data.model.ElectricTerminalsRetrofit
import com.samy.bmc.electricTerminals.data.model.ElectricTerminalsRoom
import com.samy.bmc.electricTerminals.domain.model.ElectricTerminalsDomain
import com.samy.bmc.electricTerminals.domain.repository.IElectricTerminalsRepository
import kotlinx.coroutines.flow.Flow
import retrofit2.Response
import retrofit2.await
import java.sql.Timestamp


class ElectricTerminalsRepository : IElectricTerminalsRepository {

    private val electricTerminalsDao = CustomApplication.instance.mApplicationDatabase.ElectricTerminalsDao()

    override suspend fun insertTerminalToRoom(
        terminals: ElectricTerminalsRoom
    ) {
        electricTerminalsDao.insertTerminal(terminals)
    }

    override suspend fun insertTerminals(terminals: List<ElectricTerminalsRoom>) {

        terminals.forEach {
            electricTerminalsDao.insertTerminal(it)
        }
    }

    override fun selectAll(): LiveData<List<ElectricTerminalsDomain>> {
        return electricTerminalsDao.selectAll().map {
            it.fromRoomToDomain()
        }
    }

    override suspend fun getTerminalsFromAPI(geoPoint: String) : List<ElectricTerminalsRoom> {

        val endpoint = RetrofitBuilder.getElectricTerminalsEndpoint()
        val resultRetrofit = endpoint.getElectricTerminals(geofilterDistance=geoPoint).await()
        return resultRetrofit.toRoomObject().distinct()
    }
}