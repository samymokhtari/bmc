package com.samy.bmc.electricTerminals.data.local

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.*
import com.samy.bmc.electricTerminals.data.model.ElectricTerminalsRoom

@Dao
interface ElectricTerminalsDao {

    @Query("SELECT * FROM electric_terminals")
    fun selectAll(): LiveData<List<ElectricTerminalsRoom>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(chuckNorrisRoom: ElectricTerminalsRoom)

    @Query("DELETE FROM electric_terminals")
    fun deleteAll()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertTerminal(terminals: ElectricTerminalsRoom)

}