package com.samy.bmc.electricTerminals.domain.repository

import androidx.lifecycle.LiveData
import com.samy.bmc.architecture.RetrofitBuilder
import com.samy.bmc.electricTerminals.data.model.ElectricTerminalsRoom
import com.samy.bmc.electricTerminals.domain.model.ElectricTerminalsDomain
import kotlinx.coroutines.flow.Flow
import retrofit2.Response
import java.sql.Timestamp

interface IElectricTerminalsRepository {
    fun selectAll(): LiveData<List<ElectricTerminalsDomain>>
    suspend fun insertTerminalToRoom(terminals: ElectricTerminalsRoom)
    suspend fun insertTerminals(terminals: List<ElectricTerminalsRoom>)
    suspend fun getTerminalsFromAPI(geoPoint: String) : List<ElectricTerminalsRoom>
}