package com.samy.bmc.electricTerminals.view.model

import java.util.Date

/** Object use for Ui */
data class ElectricTerminalsUi(
    val xlongitude: Double,
    val accesRecharge: String,
    val dateMaj: String,
    val adStation: String,
    val ylatitude: Double,
)

sealed class MyObjectForRecyclerView(label : String)

data class ObjectDataHeaderSample(
    val header: String
) : MyObjectForRecyclerView(label = header)

data class ObjectDataFooterSample(
    val footer: String
) : MyObjectForRecyclerView(label = footer)

data class ObjectDataSample(
    val accesRecharge: String,
    val dateMaj: String,
    val adStation: String,
) : MyObjectForRecyclerView(label = adStation)
