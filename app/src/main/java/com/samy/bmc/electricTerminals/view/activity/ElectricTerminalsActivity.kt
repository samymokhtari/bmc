package com.samy.bmc.electricTerminals.view.activity

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.HapticFeedbackConstants
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.samy.bmc.R
import com.samy.bmc.databinding.ActivityTerminalsBinding
import com.samy.bmc.electricTerminals.view.adapter.ElectricTerminalAdapter
import com.samy.bmc.electricTerminals.view.model.ElectricTerminalsUi
import com.samy.bmc.electricTerminals.view.model.ErrorPojo
import com.samy.bmc.electricTerminals.view.model.MyObjectForRecyclerView
import com.samy.bmc.electricTerminals.view.model.ObjectDataSample
import com.samy.bmc.electricTerminals.view.viewmodel.ElectricTerminalsViewModel


class ElectricTerminalsActivity : AppCompatActivity() {

    private lateinit var adapter: ElectricTerminalAdapter
    private lateinit var binding: ActivityTerminalsBinding
    private lateinit var viewModel: ElectricTerminalsViewModel
    private val terminalsListObserver = Observer<List<MyObjectForRecyclerView>> {
        adapter.submitList(it)
    }

    private var observerException = Observer<ErrorPojo> {
        Toast.makeText(this, it.code, Toast.LENGTH_LONG).show()
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.samy.bmc.R.layout.activity_terminals)
        binding = ActivityTerminalsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Create the instance of adapter
        /*adapter = ElectricTerminalAdapter { item, view ->
            onItemClick(item, view)
        }*/

        // We define the style
        binding.recyclerView.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)

        //binding.addItemButton.setOnClickListener { addWallhavenItem() }
        //binding.deleteAllItemButton.setOnClickListener { deleteWallhavenItem() }

        viewModel = ViewModelProvider(this)[ElectricTerminalsViewModel::class.java]

        // Generate data and give it to adapter
        // Create the instance of adapter
        adapter = ElectricTerminalAdapter { item, view ->
            onItemClick(item, view)
        }
        // We set the adapter to recycler view
        binding.recyclerView.adapter = adapter

    }

    override fun onStart() {
        super.onStart()
        viewModel.electricTerminalsList.observe(this, terminalsListObserver)
    }

    override fun onStop() {
        super.onStop()
        viewModel.electricTerminalsList.removeObserver(terminalsListObserver)

    }

    private fun onItemClick(objectDataSample: ObjectDataSample, view: View) {
        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val notificationId = 1
        val channelId = "channel-01"
        val channelName = "My Channel"

        val notificationIntent = Intent(this, MapsActivity::class.java)
        notificationIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK

        val pendingIntent = PendingIntent.getActivity(this, 0, Intent(), PendingIntent.FLAG_IMMUTABLE)

        val notification = NotificationCompat.Builder(this, channelId)
            .setSmallIcon(R.drawable.ic_logo_background)
            .setContentTitle("Type de borne : ${objectDataSample.accesRecharge}")
            .setContentText("Adresse de la borne : ${objectDataSample.adStation}")
            .setContentIntent(pendingIntent)
            .setAutoCancel(true)
            .build()

        val channel =
            NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_HIGH)
        notificationManager.createNotificationChannel(channel)

        notificationManager.notify(notificationId, notification)
    }


}