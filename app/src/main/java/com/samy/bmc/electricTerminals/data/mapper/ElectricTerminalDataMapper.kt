package com.samy.bmc.electricTerminals.data.mapper

import com.samy.bmc.electricTerminals.data.model.ElectricTerminalsRetrofit
import com.samy.bmc.electricTerminals.data.model.ElectricTerminalsRoom
import com.samy.bmc.electricTerminals.domain.model.ElectricTerminalsDomain

fun ElectricTerminalsRetrofit.toRoom(): List<ElectricTerminalsRoom> {
    val rooms : MutableList<ElectricTerminalsRoom> = arrayListOf()
    records.forEach {
        val terminal = ElectricTerminalsRoom(
            xlongitude = it.fields.xlongitude,
            accesRecharge = it.fields.accesRecharge,
            dateMaj = it.fields.dateMaj,
            adStation = it.fields.adStation,
            ylatitude = it.fields.ylatitude
        )
        rooms.add(terminal)
    }
    return rooms
}

fun List<ElectricTerminalsRoom>.fromRoomToDomain(): List<ElectricTerminalsDomain> {
    return map {
        ElectricTerminalsDomain(
            xlongitude = it.xlongitude,
            ylatitude = it.ylatitude,
            dateMaj = it.dateMaj,
            adStation = it.adStation,
            accesRecharge = it.accesRecharge
        )
    }
}

fun ElectricTerminalsRetrofit.toRoomObject(): List<ElectricTerminalsRoom> {
    return records.map {
        ElectricTerminalsRoom(
            xlongitude = it.fields.xlongitude,
            ylatitude = it.fields.ylatitude,
            accesRecharge = it.fields.accesRecharge,
            dateMaj = it.fields.dateMaj,
            adStation = it.fields.adStation
        )
    }
}