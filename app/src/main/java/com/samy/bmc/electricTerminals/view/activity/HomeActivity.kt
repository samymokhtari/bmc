package com.samy.bmc.electricTerminals.view.activity

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import com.firebase.ui.auth.AuthUI
import com.samy.bmc.R
import com.samy.bmc.databinding.ActivityMainBinding
import com.samy.bmc.electricTerminals.view.viewmodel.ElectricTerminalsViewModel


class HomeActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var viewModel: ElectricTerminalsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)

        setContentView(binding.root)
        viewModel = ViewModelProvider(this)[ElectricTerminalsViewModel::class.java]

        binding.btnGoToElectricTerminals.setOnClickListener {
            goToElectricTerminalActivity()
        }

        binding.btnGoToMaps.setOnClickListener {
            goToMapActivity()
        }

        binding.btnLogOut.setOnClickListener {
            // Create and launch sign-in intent
            AuthUI.getInstance()
                .signOut(this)
                .addOnCompleteListener {
                    goToLoginActivity()
                }
        }

        try {
            if (ContextCompat.checkSelfPermission(
                    applicationContext,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    101
                )
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun goToMapActivity() {
        startActivity(Intent(this, MapsActivity::class.java))
    }

    private fun goToLoginActivity() {
        startActivity(Intent(this, LoginActivity::class.java))
    }

    private fun goToElectricTerminalActivity() {
        startActivity(Intent(this, ElectricTerminalsActivity::class.java))
    }
}