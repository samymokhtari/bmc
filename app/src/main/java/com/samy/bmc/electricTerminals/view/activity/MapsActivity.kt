package com.samy.bmc.electricTerminals.view.activity

import android.Manifest
import android.app.PendingIntent.getActivity
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.*
import com.google.android.gms.maps.CameraUpdateFactory.newCameraPosition
import com.google.android.gms.maps.model.*
import com.google.android.gms.tasks.OnSuccessListener
import com.samy.bmc.electricTerminals.view.model.ElectricTerminalsUi
import com.samy.bmc.electricTerminals.view.viewmodel.ElectricTerminalsViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class MapsActivity : AppCompatActivity(), OnMapReadyCallback {
    private lateinit var  mMapView : MapView
    private lateinit var  mLocationManager : LocationManager
    private lateinit var fusedLocationProvider : FusedLocationProviderClient
    private lateinit var viewModel: ElectricTerminalsViewModel


    private val observer = Observer<List<ElectricTerminalsUi>> {}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.samy.bmc.R.layout.activity_maps)
        mMapView = (findViewById(com.samy.bmc.R.id.map))
        mMapView.onCreate(savedInstanceState);

        mMapView.getMapAsync(this)

        mLocationManager = getSystemService(LOCATION_SERVICE) as (LocationManager)
        fusedLocationProvider = LocationServices.getFusedLocationProviderClient(this)

        // Instantiate viewModel
        viewModel = ViewModelProvider(this)[ElectricTerminalsViewModel::class.java]
    }

    override fun onMapReady(map: GoogleMap) {
        getLastLocation(map)
        map.mapType = GoogleMap.MAP_TYPE_NORMAL;
        map.uiSettings.isZoomControlsEnabled = true;
        map.uiSettings.isCompassEnabled = true;
        map.uiSettings.isScrollGesturesEnabled = true;
        map.uiSettings.isScrollGesturesEnabledDuringRotateOrZoom = true;

        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            map.isMyLocationEnabled = true;
            map.uiSettings.isMyLocationButtonEnabled = true;
        }

        viewModel.electricTerminalsLiveData.observe(this, Observer { electricTerminals ->
            // Update UI with the electricTerminals list
            electricTerminals.forEach {
                map.addMarker(
                    MarkerOptions()
                        .position(LatLng(it.ylatitude,it.xlongitude))
                        .title(it.adStation)
                        .snippet("Date de mise à jour : ${it.dateMaj}")
                        .flat(true)
                        .icon(
                            BitmapDescriptorFactory
                            .defaultMarker(BitmapDescriptorFactory.HUE_GREEN))

                )
            }
        })
        when (applicationContext.resources?.configuration?.uiMode?.and(Configuration.UI_MODE_NIGHT_MASK)) {
            Configuration.UI_MODE_NIGHT_YES -> {
                map.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, com.samy.bmc.R.raw.map));
            }
            Configuration.UI_MODE_NIGHT_NO -> {
                map.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, com.samy.bmc.R.raw.map_light));
            }
            Configuration.UI_MODE_NIGHT_UNDEFINED -> {
                map.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, com.samy.bmc.R.raw.map_light));
            }
        }

    }


    private fun getLastLocation(map: GoogleMap) {
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
            == PackageManager.PERMISSION_GRANTED)  {
            fusedLocationProvider.lastLocation
                .addOnSuccessListener(OnSuccessListener<Location> {
                        location ->
                    if (location != null) {
                        viewModel.initializeTerminals(LatLng(location.latitude, location.longitude), 1000000)
                        val latLng = LatLng(location.latitude, location.longitude)
                        val cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 14.4746F)
                        map.animateCamera(cameraUpdate)
                    } else {
                        viewModel.initializeTerminals(LatLng(49.836895,3.299732),1000000)
                        //callback.onCallback(Status.SUCCESS, location)
                        Log.i("loc", "empty location")
                    }
                })
                .addOnFailureListener {
                    Log.e("err", it.message.toString())
                }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
        mMapView.onCreate(savedInstanceState)
    }

    override fun onStart() {
        super.onStart()
        mMapView.onStart()

    }

    override fun onResume() {
        super.onResume()
        mMapView.onResume()
    }

    override fun onStop() {
        viewModel.electricTerminalsLiveData.removeObserver(observer)
        super.onStop()
        mMapView.onStop()
    }

    override fun onDestroy() {
        super.onDestroy()
        //mMapView.onDestroy()
    }

    override fun onPause() {
        super.onPause()
        mMapView.onPause()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        //mMapView.onSaveInstanceState(outState)
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mMapView.onLowMemory()
    }
}