package com.samy.bmc.electricTerminals.view.mapper

import com.samy.bmc.electricTerminals.domain.model.ElectricTerminalsDomain
import com.samy.bmc.electricTerminals.view.model.*

fun List<ElectricTerminalsDomain>.fromDomainToUi(): List<ElectricTerminalsUi> {
    return map {
        ElectricTerminalsUi(
            xlongitude = it.xlongitude,
            ylatitude = it.ylatitude,
            accesRecharge = it.accesRecharge,
            adStation = it.adStation,
            dateMaj = it.dateMaj
        )
    }
}

fun ElectricTerminalsDomain.toUi(): ObjectDataSample {
    return ObjectDataSample(
        accesRecharge = accesRecharge,
        adStation = adStation,
        dateMaj = dateMaj
    )
}

fun List<ObjectDataSample>.toMyObjectForRecyclerView(): List<MyObjectForRecyclerView> {
    val result = mutableListOf<MyObjectForRecyclerView>()

    groupBy {
        // Split in 2 list, modulo and not
        it.dateMaj
    }.forEach { (dateMaj, items) ->
        // For each mean for each list split
        // Here we have a map (key = isModulo) and each key have a list of it's items
        result.add(ObjectDataHeaderSample("Date de mise à jour : $dateMaj"))
        result.addAll(items)
        result.add(ObjectDataFooterSample("Somme : ${items.count()}"))
    }
    return result
}