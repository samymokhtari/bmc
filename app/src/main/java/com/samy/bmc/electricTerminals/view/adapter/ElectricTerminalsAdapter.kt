package com.samy.bmc.electricTerminals.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.Recycler
import com.samy.bmc.databinding.ItemCustomRecyclerBinding
import com.samy.bmc.databinding.ItemCustomRecyclerViewFooterBinding
import com.samy.bmc.databinding.ItemCustomRecyclerViewHeaderBinding
import com.samy.bmc.electricTerminals.view.model.*

private val diffItemUtils = object : DiffUtil.ItemCallback<MyObjectForRecyclerView>() {

    override fun areItemsTheSame(oldItem: MyObjectForRecyclerView, newItem: MyObjectForRecyclerView): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: MyObjectForRecyclerView, newItem: MyObjectForRecyclerView): Boolean {
        return oldItem == newItem
    }
}

class ElectricTerminalAdapter(
    private val onItemClick: (objectDataSample :ObjectDataSample, view: View) -> Unit,
) : ListAdapter<MyObjectForRecyclerView, RecyclerView.ViewHolder>(diffItemUtils) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        when (viewType) {
            MyItemType.ROW.type -> {
                ElectricTerminalsViewHolder(
                    ItemCustomRecyclerBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent,
                        false
                    ), onItemClick
                )
            }
            MyItemType.HEADER.type -> {
                ElectricTerminalsHeaderViewHolder(
                    ItemCustomRecyclerViewHeaderBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent,
                        false
                    )
                )
            }
            MyItemType.FOOTER.type -> {
                ElectricTerminalsFooterViewHolder(
                    ItemCustomRecyclerViewFooterBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent,
                        false
                    )
                )
            }
            else -> throw RuntimeException("Wrong view type received $viewType")
        }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder.itemViewType) {
            MyItemType.ROW.type -> (holder as ElectricTerminalsViewHolder).bind(getItem(position) as ObjectDataSample)
            MyItemType.HEADER.type -> (holder as ElectricTerminalsHeaderViewHolder).bind(
                getItem(
                    position
                ) as ObjectDataHeaderSample
            )
            MyItemType.FOOTER.type -> (holder as ElectricTerminalsFooterViewHolder).bind(
                getItem(
                    position
                ) as ObjectDataFooterSample
            )
            else -> throw RuntimeException("Wrong view type received ${holder.itemView}")

        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (getItem(position)) {
            is ObjectDataSample -> MyItemType.ROW.type
            is ObjectDataHeaderSample -> MyItemType.HEADER.type
            is ObjectDataFooterSample -> MyItemType.FOOTER.type
        }
    }



}



enum class MyItemType(val type: Int) {
    ROW(0),
    HEADER(1),
    FOOTER(3)
}
