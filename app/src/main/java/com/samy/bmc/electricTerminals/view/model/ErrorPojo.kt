package com.samy.bmc.electricTerminals.view.model

data class ErrorPojo (
    val code: Int,
    val name: String
)
