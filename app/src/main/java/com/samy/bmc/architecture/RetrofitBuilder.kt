package com.samy.bmc.architecture

import com.google.gson.GsonBuilder
import com.samy.bmc.electricTerminals.data.remote.IElectricTerminalsEndpoint
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


object RetrofitBuilder {
    private const val BASE_URL = "https://odre.opendatasoft.com/api/records/1.0/"

    // Create an instance of retrofit
    private val retrofit: Retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
        .build()

    fun getElectricTerminalsEndpoint(): IElectricTerminalsEndpoint {
        return retrofit.create(IElectricTerminalsEndpoint::class.java)
    }


}

