package com.samy.bmc.architecture

import androidx.room.Database
import androidx.room.RoomDatabase
import com.samy.bmc.electricTerminals.data.local.ElectricTerminalsDao
import com.samy.bmc.electricTerminals.data.model.ElectricTerminalsRoom

@Database(
    entities = [
        ElectricTerminalsRoom::class
    ],
    version = 1,
    exportSchema = false
)
abstract class CustomRoomDatabase : RoomDatabase() {
    // Here goes the list of all DAO needed
    abstract fun ElectricTerminalsDao(): ElectricTerminalsDao
}